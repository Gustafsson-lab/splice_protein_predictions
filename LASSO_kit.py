#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 13:43:35 2017

@author: rasmus
"""

import sklearn.linear_model as lm
import numpy as np


def doLASSO(TFs, targets, alpha_inparam):
    """
    Here we can do the lasso with a l1 penalty of weight 'alpha'.
    """
    model = lm.Lasso(alpha=alpha_inparam)
    model.fit(TFs,targets)
    coef = model.coef_
    return coef

def doLASSOcv(TFs, targets, n_workers_=-1):
    model = lm.LassoCV(cv=len(targets), n_jobs=n_workers_)
    model.fit(TFs, targets)
    coef = model.coef_
    return coef



def doLASSO_1SE(X, Y, alphas=(np.linspace(0,2)**2), nSTD=1, return_prediction=False ):
    """
    This is a function that takes the sklearn implementation of LASSO, and
    performs a leave one out cross validation. As compared to the LassoCV
    function in sklearn.linear_model, here we take the point of the sparsest 
    model that is within one standard deviation of best cross validation as
    dependent on alpha.
    
    Usage:
            _1se_coef = doLASSO_1SE(X, Y, alphas=(np.linspace(0,2)**2) )
            
    Where:
        X are the explanatory variables
        Y are the target entities
        alphas are the sparsity coefficients of the l1-term. Usually denoted 
            lambda
    
    Returns:
        _1se_coef which are the parameter yielding the sparsest model within
            one standard deviation of the best of the cv-points 
    """
    def lasso_internal(X, Y, alphas):
        coef = np.empty((len(alphas), len(X[0,:])))
        coef[:] = np.NaN
    
        for alpha,i in zip(alphas, range(len(alphas))):
            model = lm.Lasso(alpha=alpha)
            model.fit(X,Y)
            coef[i,:] = model.coef_
        return coef

    nObs = len(Y)
    nAlpha = len(alphas)
    costs = np.empty( (nObs, nAlpha))
    costs[:] = np.NaN
    predictions = costs.copy()
    
    for i in range(nObs):
        Yshort = Y[np.arange(0,nObs) != i]
        Xshort = X[np.arange(0,nObs) != i, :]
        params = lasso_internal(Xshort,Yshort,alphas)
        for j in range(nAlpha):
            costs[i,j] = Y[i] - np.dot(params[j,:],X[i,:])
            predictions[i,j] = np.dot(params[j,:],X[i,:])
            
    costs = costs**2
    mean_ = np.mean(costs,axis=0)
    std_ = np.std(costs[:, np.argmin(mean_)])
    is_smaller = mean_ < (np.min(mean_)+(nSTD*std_))
    best_alpha = alphas[is_smaller][-1]
    _1se_coef = lasso_internal(X,Y, [best_alpha])

    if not return_prediction:        
        return _1se_coef
    elif return_prediction:
        _1SE_predictions = predictions[:,is_smaller][:,-1]
        return _1se_coef, _1SE_predictions
    
    

def doLASSO_nParam(X, Y, nParams=2, alpha_0=0, step=0.04, exp=2, nSTD=1, recursive_startsearch=True, return_prediction=False ):
    """
    This is a function that takes the sklearn implementation of LASSO, and
    performs a leave one out cross validation. As compared to the LassoCV
    function in sklearn.linear_model, here we take the point of the sparsest 
    model that is within one standard deviation of best cross validation as
    dependent on alpha.
    
    Usage:
            _1se_coef = doLASSO_1SE(X, Y, alphas=(np.linspace(0,2)**2) )
            
    Where:
        X are the explanatory variables
        Y are the target entities
        alphas are the sparsity coefficients of the l1-term. Usually denoted 
            lambda
        alpha_0 = the smallest lambda value tested
        step, exp = how much to increase lambda according to 
            lambda = alpha_0 + (step*iteration)**exp
    
    Returns:
        _1se_coef which are the parameter yielding the sparsest model within
            one standard deviation of the best of the cv-points 
    """
    
    def lasso_internal(X, Y, alpha):
        model = lm.Lasso(alpha=alpha)
        model.fit(X,Y)
        return model.coef_

    def search_param(X, Y, alpha_0, step, exp, ):
        i=0
        params_tmp = np.ones(X.shape[1])
        alpha = 0 # Declare if loop below is never entered
    
        while np.sum(params_tmp != 0) > np.min((nParams, nObs)):
            alpha = alpha_0 + (step*i)**exp
            params_tmp = lasso_internal(X,Y,alpha)
            i+=1
        return alpha, params_tmp, i 
    
    nObs = len(Y)
    costs = []
    
    # Here, we do a first search for finding a start lambda that gives the 
    # right amount of parameters
    alpha, params_tmp, i = search_param(X, Y, alpha_0, step, exp, )
    
    if np.all(params_tmp == 0) & recursive_startsearch:
        while True:
            alpha_0 = alpha = alpha_0 + (step*(i-2))**exp
            step = step/2
            alpha, params_tmp, i = search_param(X, Y, alpha_0, step, exp, )
            if np.sum(params_tmp != 0) == np.min((nParams, nObs)):
                break
        
    alpha_0 = alpha
    used_alphas = []
    iteration = 0
    
    while np.sum(params_tmp != 0) > 0:
        cost_tmp = []
        alpha = alpha_0 + (step*iteration)**exp
        used_alphas.append(alpha)
        for i in range(nObs):
            Yshort = Y[np.arange(0,nObs) != i]
            Xshort = X[np.arange(0,nObs) != i, :]
            params_tmp = lasso_internal(Xshort,Yshort,alpha)
            cost_tmp.append((Y[i] - np.dot(params_tmp,X[i,:]))**2)
        params_tmp = lasso_internal(X,Y,alpha)
        costs.append(cost_tmp)
        iteration += 1
    
    used_alphas = np.array(used_alphas)
    costs = np.array(costs)
    mean_cost = costs.mean(axis=1)
    min_pos = np.argmin(mean_cost)
    is_smaller = mean_cost
    std_ = np.std(costs[np.argmin(mean_cost),:])
    is_smaller = mean_cost < (np.min(mean_cost)+(nSTD*std_))

    min_alpha = used_alphas[min_pos]
    min_std_alpha = used_alphas[is_smaller][-1]
    
    coef_min = lasso_internal(X,Y,min_alpha)
    coef_SE = lasso_internal(X,Y,min_std_alpha)
    
    

    if not return_prediction:        
        return coef_min, coef_SE
    elif return_prediction:
        pred_min = np.dot(X, coef_min)
        pred_SE =  np.dot(X, coef_SE)

        return coef_min, coef_SE, pred_min, pred_SE
    