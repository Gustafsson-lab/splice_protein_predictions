#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 08:11:52 2018

@author: rasmus

This is a set of functions that are used when building an interpolated look-up
table of a data-set
"""


__author__ = 'Rasmus Magnusson'
__COPYRIGHT__ = 'Rasmus Magnusson, 2017, Linköping'

import numpy as np

def build_interp_pat(times, vals, points=100):
    
    def interp_vals(times, vals, nPoints=100):
        # This function performs does the actual interpolation 
        assert np.all(np.diff(times) > 0 )
        assert times[0] == 0
        qTP = np.linspace(0, times[-1], nPoints)
        return np.interp(qTP, times, vals)
    
    mat = np.empty( [len(vals[:,0]), points] )
    
    for i in range(len(vals[:,0])):
        mat[i,:] = interp_vals(times, vals[i,:], nPoints=points)
    return mat

def get_vals(tp1, tp2, nPoints):
    scale = (nPoints/tp1[-1])
    start_points = tp2*scale
    
    # Due to the long time span in the protein measurements, we will get the
    # same case every time we cross the 24h time point in the sliding. 
    # Therefore, we can stop after the point crosses this level
    max_vals = nPoints / (tp1[-1]/tp2[-1])
    
    return start_points.astype(int), int(max_vals*1.1)
