#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os

import sklearn.linear_model as lm
import scipy.stats as sts
import pickle as pkl
import numpy as np
import interp_funs
import LASSO_kit
import os.path

from multiprocessing.pool import Pool
from functools import partial

import warnings
warnings.filterwarnings("ignore")
def lasso(X, Y, alphas):
    coef = np.empty((len(alphas), len(X[0,:])))
    coef[:] = np.NaN

    for alpha,i in zip(alphas, range(len(alphas))):
        model = lm.ElasticNet(alpha=alpha)
        model.fit(X,Y)
        coef[i,:] = model.coef_
    return coef

def loocv_model(X, Y, alphas=(np.linspace(0,2)**2) ):
    nObs = len(Y)
    nAlpha = len(alphas)
    costs = np.empty( (nObs, nAlpha))
    costs[:] = np.NaN
    best_obs = np.empty(Y.shape)
    best_obs[:] = np.NaN

    for i in range(nObs):
        Yshort = Y[np.arange(0,nObs) != i]
        Xshort = X[np.arange(0,nObs) != i, :]
        params = lasso(Xshort,Yshort,alphas)
        for j in range(nAlpha):
            costs[i,j] = Y[i] - np.dot(params[j,:],X[i,:])

        # Here, we calculate the best parameter set to
        best_obs[i] = np.dot(params[np.argmin(costs[i,:]**2),:],X[i,:])
    return costs, best_obs

def do_corr(prot, targets):
    rho, p = sts.pearsonr(prot, targets)
    return rho


def cross_val(tmp_trans, prot_tmp):
    nObs = len(prot_tmp)
    cost_min = []
    cost_1SE=[]
    for i in range(nObs):
        Yshort = prot_tmp[np.arange(0,nObs) != i]
        Xshort = tmp_trans[np.arange(0,nObs) != i, :]
        coef_min, coef_SE, pred_min, pred_SE = LASSO_kit.doLASSO_nParam(Xshort, Yshort, nParams=2, alpha_0=0, step=0.04, exp=2, nSTD=1, return_prediction=True )
        cost_min.append((prot_tmp[i] - np.dot(coef_min,tmp_trans[i,:]))**2)
        cost_1SE.append((prot_tmp[i] - np.dot(coef_SE,tmp_trans[i,:]))**2)
    return cost_min, cost_1SE

def parfun_lasso(prot_names,trans, prot_mat, start_points, tp_set):
    prot_mat = prot_mat[:, start_points + tp_set]

    rho_dict = {}
    rho_dict_1SE = {}
    names = {}
    coef_dict = {}
    coef_dict_1SE = {}
    R2_dict = {}
    time_cv_min = {}
    time_cv_1SE = {}

    for i in range(len(prot_names)):
        prot_name = prot_names[i]

        tmp_trans = trans.iloc[ np.in1d(trans.ACCNUM, prot_name),:-1].values.T
        prot_tmp = prot_mat[i,:]

        coef_min, coef_SE, pred_min, pred_SE = LASSO_kit.doLASSO_nParam(tmp_trans, prot_tmp, nParams=2, alpha_0=0, step=0.04, exp=2, nSTD=1, return_prediction=True )
        time_cv_cost_min, time_cv_cost_1SE = cross_val(tmp_trans, prot_tmp)

        time_cv_min.update({prot_name:time_cv_cost_min})
        time_cv_1SE.update({prot_name:time_cv_cost_1SE})

        rho = do_corr(prot_tmp, pred_min)
        _1se_rho = do_corr(prot_tmp, pred_SE)

        rho_dict.update({prot_name:rho})
        rho_dict_1SE.update({prot_name:_1se_rho})

        names.update({prot_name:trans.index[ np.in1d(trans.ACCNUM, prot_name)]})
        rho_dict.update({prot_name:rho})
        rho_dict_1SE.update({prot_name:_1se_rho})

        coef_dict.update({prot_name:coef_min})
        coef_dict_1SE.update({prot_name:coef_SE})

        SSres = np.sum(((np.dot(tmp_trans, coef_min) + np.mean(prot_tmp)) - prot_tmp)**2)
        SSerr = np.sum((np.mean(prot_tmp) - prot_tmp)**2)
        R2 = 1 - (SSres/SSerr)
        R2_dict.update({prot_name:R2})

    pkl.dump([rho_dict, rho_dict_1SE, names, coef_dict, coef_dict_1SE, R2_dict, time_cv_min, time_cv_1SE], open('results/results_of_delay' + str(tp_set) + '.p', 'wb'))




def main():


    print('Loads file from pickle...')
    prot, trans, tp_prot, tp_trans = pkl.load( open('data.p', 'rb'))

    np.random.seed(2305)

    nPoints = 10000
    prot_mat = interp_funs.build_interp_pat(tp_prot, prot.values, nPoints)
    start_points, max_val = interp_funs.get_vals(tp_prot.copy(), tp_trans.copy(), nPoints)

    p = Pool(4)
    time_delay = np.unique((np.linspace(0, np.sqrt(2001), 200)**2).astype(int))

    p.map(partial(parfun_lasso, prot.index, trans, prot_mat, start_points), time_delay)
    p.close()

if __name__ == '__main__':
    main()

