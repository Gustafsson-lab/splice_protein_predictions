Biomarker predictions from mRNA splice variant to protein prediction models
================
Summary of study
----------------
Profiling of mRNA expression is an important method to identify biomarkers but complicated by limited correlations between mRNA expression and protein abundance. We hypothesised that these correlations could be improved by mathematical models based on measuring splice variants and time delay in protein translation. We characterised time-series of primary human naïve CD4+ T cells during early T-helper type 1 differentiation with RNA-sequencing and mass-spectrometry proteomics. We then performed computational time-series analysis in this system and in two other key human and murine immune cell types. Linear mathematical mixed time-delayed splice variant models were used to predict protein abundances, and the models were validated using out-of-sample predictions. Lastly, we re-analysed RNA-seq datasets to evaluate biomarker discovery in five T-cell associated diseases, further validating the findings for multiple sclerosis (MS) and asthma.

Approach description
----------------
This study used gene-specific splice-to-protein to predict disease-relevant markers. The genetic splice variants were chosen to minimize the L1-constrained least square regression of a linear model, as written below

```math
\min_{\beta_j} \sum_i(y_{i,j} - \sum_k \beta_{j,k}*x_{i,k})^2  + \lambda \sum_k |\beta_{j,k}| 
```

Where the j:th protein y is predicted by a linear combination of the k splice variants x. The time points are denoted by i, and the parameter lambda was chosen to minimize the leave-one-out cross-validation error. 


Code description
----------------
The project was carried out in Python 3, mainly using the Numpy, SKlearn, Scipy and Pandas packages. The models were built using 'main.py', which calls the LASSO_kit.py function in a parallel loop, such that each call corresponds to one specific time delay. Moreover,  main.py also loads the included interp_funs module for the linear interpolation of data. 

To analyse the models' predictive performances using splice variants, we used an independent leave-one-out cross-validation without the time delay. These results, along with the plotting code for other relevant figures, were created using the 'main_cv_over_genes.py' file. 

The main finding of this study was that splice variant models could be used to successfully predict biomarkers in a set of diseases. These diseases were analysed in the file 'main_biomarkers.py', according to the following setup. First, the model is read (line 221). Second, the splice variants of each dataset were loaded (line 133 of subfunction called at line 224). Third, at line 139-140, the splice variants were combined according to the respective coefficients in the linear model. Fourth, a non-parametric Kruskal-Wallis test was used at lines 150-152 to estimate the probability of differential expression between the (predicted/measured) values of the control and case groups. Lastly, the results are plotted at lines 188-215.

