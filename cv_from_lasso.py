#!/usr/bin/env python3
# -*- coding: utf-8 -*-




import pickle as pkl
import numpy as np
import os
import pandas as pd
import scipy.stats as sts
import seaborn as sns
import matplotlib.pyplot as plt
import interp_funs

def get_gene_td_optima(mean_abo,j, cv_all): #abo = all but one ;)
    args = np.argmin(mean_abo,1)

    pred_resid = []
    for i, pos in enumerate(args):
        pred_resid.append(cv_all[pos].iloc[i,j])
    return pred_resid


def test_corr_pred(X, Y):
    pred = []
    for i in range(len(Y)):
        Xshort = X[:, np.arange(len(Y)) != i]
        Yshort = Y[np.arange(len(Y)) != i]

        them_corrs_yo_tmp_cool_var_no = []
        for j in range(X.shape[0]):
            them_corrs_yo_tmp_cool_var_no.append(sts.pearsonr(Xshort[j], Yshort)[0])
        pos = np.argmax(them_corrs_yo_tmp_cool_var_no)
        pred.append(X[pos,i])
    return pred

def get_mRNA_data(prot_names):
    mRNA = pd.read_csv('../data/mRNA_expression.csv')
    mRNA = mRNA.set_index('Entrez')
    f = lambda x: (x - np.nanmean(x))/np.nanstd(x)
    mRNA = mRNA.apply(f,axis=1)
    key = pd.read_csv('key_uniprot_entrez.csv').iloc[:,1:].set_index('ENTREZID')
    key = key[key.UNIPROT.isin(prot_names)]
    return pd.merge(mRNA, key, left_index=True, right_index=True).set_index('UNIPROT')


def CLT_binom(vals, p=0.5):
    def get_p(z):
        return (.5*np.log(2*np.pi) - 0.5*z*z - np.log(z) + np.log(1 - z**-2 + 3*z**-4))/(np.log(10))
    n = len(vals)
    mu = n*p
    sigma = np.sqrt(mu*(1-p))
    Z = (vals.sum() - mu)/sigma
    return get_p(Z)


def main():
    res_dir = 'results/pickles_CV_pearson/'
    pkl_savename = 'results_of_delay'
    prot, trans, tp_prot, tp_trans = pkl.load( open('data.p', 'rb'))

    res = os.listdir(res_dir)

    numbs = [int(X.split('delay')[-1].split('.')[0]) for X in res]
    numbs = np.sort(numbs)


    ref_seq = None
    std = [[] for i in range(6)]
    mean = [[] for i in range(6)]
    cv_all = []
    for n in numbs:
        with open(res_dir + pkl_savename + str(n) + '.p', 'rb') as f:
            __, __, __, __, __, __, time_cv_min, __ = pkl.load(f)

        cv = pd.DataFrame(time_cv_min).transpose()
        if not ref_seq is None:
            assert np.all(ref_seq == cv.index)
            ref_seq = cv.index
        else:
            ref_seq = cv.index
        for i in range(6):
            mean[i].append(cv.iloc[:, np.arange(6) != i].mean(1).values)
            std[i].append(cv.iloc[:, np.arange(6) != i].std(1).values)
        cv_all.append(cv)

    pred = []
    for j in range(6):
        pred.append(get_gene_td_optima(np.array(mean[j]).T, j, cv_all))

    pred = np.array(pred).T

    pred_no_delay = cv_all[0]

    plt.plot()
    a = [X.mean(1).values for X in cv_all]
    a = np.array(a).T
    time_delay = np.unique((np.linspace(0, np.sqrt(2001), 200)**2).astype(int))
    plt.hist(24*(time_delay/2000)[np.argmin(a,1)])
    plt.savefig('img_time_cv/time_delay.svg')

    # Now do the same thing with the correlation
    trans = trans.set_index('ACCNUM')

    nPoints = 10000
    prot_mat = interp_funs.build_interp_pat(tp_prot, prot.values, nPoints)
    start_points, max_val = interp_funs.get_vals(tp_prot.copy(), tp_trans.copy(), nPoints)

    best_corr = {}
    gene_vs_prot = {}
    Z = lambda X: (X - np.mean(X))/np.std(X)
    gdata = get_mRNA_data(prot.index)
    for i, GNAME in enumerate(prot.index):
        Ydata_tmp_no_td = Z(prot_mat[prot.index == GNAME, start_points])
        trans_tmp = trans.loc[GNAME]
        if not len(trans_tmp.shape) != 2:
            Xdata_tmp = trans_tmp.apply(Z,1)
            unseen_corr = test_corr_pred(Xdata_tmp.values, Ydata_tmp_no_td)
            best_corr.update({GNAME:(unseen_corr - Ydata_tmp_no_td)**2})

        if np.sum(gdata.index == GNAME) == 1:
            gene_vs_prot.update({GNAME:(Z(gdata.loc[GNAME].values) - Ydata_tmp_no_td)**2})

    GP_df = pd.DataFrame(gene_vs_prot).transpose()
    corr_df = pd.DataFrame(best_corr).transpose()

    pred_no_delay_is_better = ((pred_no_delay- corr_df.loc[ref_seq]) < 0).values.sum()
    n_is_not_nan = np.prod(corr_df.loc[ref_seq].shape) -np.isnan(corr_df.loc[ref_seq]).sum().sum()
    delay_is_better_than_no_delay = ((pred - pred_no_delay) < 0).values.sum()
    n_delay = np.prod(pred.shape)

    delay_is_better_than_no_delay_tmp = pred - pred_no_delay
    tmp = pd.read_csv('is_early.csv').set_index('Unnamed: 0')
    tmp = tmp.iloc[np.argsort(tmp.iloc[:,0]),:]
    plt.figure()
    delay_is_better_than_no_delay_tmp .loc[tmp.index].mean(1).plot()
    plt.savefig('img_time_cv/as_func_of_time_delay.svg')


    plt.figure()
    plt.plot(tmp.iloc[:,0],delay_is_better_than_no_delay_tmp.loc[tmp.index].mean(1),'*')
    plt.savefig('img_time_cv/as_func_of_time_delay_v2.svg')


    delay_is_better_than_best_splice = ((pred - corr_df.loc[ref_seq]) < 0).values.sum()



    p1 = sts.binom_test(delay_is_better_than_no_delay,n_delay, alternative='greater')
    p2 = sts.binom_test(pred_no_delay_is_better, n_is_not_nan, alternative='greater')
    sts.binom_test(pred_no_delay_is_better, n_is_not_nan, alternative='greater')

    delay_is_better_than_no_delay/n_delay
    pred_no_delay_is_better/n_is_not_nan
    delay_is_better_than_best_splice/n_is_not_nan

# =============================================================================
#     # Also do a non-parametric test
#     sts.kruskal(np.pred.flatten(), pred_no_delay.values.flatten())
#     sts.kruskal(pred_no_delay.values.flatten(), corr_df.values.flatten())
#     sts.kruskal(corr_df.values.flatten(), GP_df.values.flatten())
# =============================================================================

    """ Here we can approximate the p-value by assuming two props. First, we
    can use the central limit theorem that large binom follows a normal dist
    where
        mu = np
        sigma = sqrt(np(1-p))

    next we approximate the p val by

    """
    comb_better_vals = (pred_no_delay - corr_df.loc[ref_seq])
    comb_better_vals  = comb_better_vals .values.flatten()
    comb_better_vals  = (comb_better_vals  < 0)[~np.isnan(comb_better_vals  )]


    time_better = CLT_binom(delay_is_better_than_no_delay_tmp.values.flatten() < 0)
    comb_better = CLT_binom(comb_better_vals)
    best_corr_better = CLT_binom(((corr_df.loc[ref_seq] - GP_df.loc[ref_seq]) < 0).values.flatten() )

    with open('results_cv_from_lasso.txt', 'w') as f:
        f.write('n pred is higher than best splice: ' +str(pred_no_delay_is_better))
        f.write(' out of: ' + str(n_is_not_nan))
        f.write(' which is: ' + str(pred_no_delay_is_better/n_is_not_nan) + '%\n')
        f.write('P: ' + str(p2))
        f.write('\n\nn pred with time is higher than pred: ' +str(delay_is_better_than_no_delay))
        f.write(' out of: ' + str(n_delay))
        f.write(' which is: ' + str(delay_is_better_than_no_delay/n_delay) + '%\n')
        f.write('P: ' + str(p1))
        f.write('\n\n\n\n')
        f.write('P-values when approximating the p-values via the CLT')
        f.write('\nBest corr better than gene: ' + str(best_corr_better))
        f.write('\nCombination of splice better than corr: ' + str(comb_better))
        f.write('\nCombination of splice with time better than just combinations: ' + str(time_better))



    n_best_corr_better =  ((corr_df.loc[ref_seq] - GP_df.loc[ref_seq]) < 0).values.sum()
    n_corr_better = np.prod(corr_df.loc[ref_seq].shape) - np.isnan(corr_df.loc[ref_seq] - GP_df.loc[ref_seq]).values.sum()
    model_better = ((pred_no_delay - GP_df.loc[ref_seq]) < 0).values.sum()
    n_model_better = np.prod(pred_no_delay.shape) - np.isnan(pred_no_delay - GP_df.loc[ref_seq]).values.sum()
    model_time_better_than_gene = ((pred - GP_df.loc[ref_seq]) < 0).values.sum()
    model_better_than_corr = ((pred_no_delay - corr_df.loc[ref_seq]) < 0).values.sum()
    n_model_better_than_corr = np.prod(pred_no_delay.shape) - np.isnan(pred_no_delay - corr_df.loc[ref_seq]).values.sum()
    model_time_better_than_corr = ((pred - corr_df.loc[ref_seq]) < 0).values.sum()
    model_time_better_than_model = ((pred - pred_no_delay) < 0).values.sum()
    n_time_better_than_model = np.prod(pred.shape)

    p_corr_gene = sts.binom_test(n_best_corr_better,n_corr_better, alternative='greater')
    p_model_gene = CLT_binom(((pred_no_delay - GP_df.loc[ref_seq]) < 0).values.flatten()[~np.isnan(((pred - GP_df.loc[ref_seq]) < 0).values).flatten()])
    p_model_time_gene = CLT_binom(((pred - GP_df.loc[ref_seq]) < 0).values.flatten()[~np.isnan(((pred - GP_df.loc[ref_seq]) < 0).values).flatten()])
    p_model_better_than_corr = sts.binom_test(model_better_than_corr,n_model_better, alternative='greater')
    p_model_time_better_than_corr = CLT_binom(((pred - corr_df.loc[ref_seq]) < 0).values.flatten()[~np.isnan(((pred - corr_df.loc[ref_seq]) < 0).values.flatten())])
    p_model_time_better_than_model = CLT_binom(((pred - pred_no_delay) < 0).values.flatten())


    with open('results_cv_from_lasso_Th1_table.txt', 'w') as f:
        f.write('Here are the results of the double cross validation\n\n')
        f.write('best correlating splice better than gene : n= ' + str(n_best_corr_better) + ' of: ' + str(n_corr_better) + ' p = ' +str(p_corr_gene ) )
        f.write('\nsplice combination better than gene : n= ' + str(model_better) + ' of: ' + str(n_model_better) + ' p = ' +str(p_model_gene) )
        f.write('\nsplice combination with time better than gene : n= ' + str(model_time_better_than_gene ) + ' of: ' + str(n_model_better) + ' p = ' +str(p_model_time_gene ) )
        f.write('\nsplice combination better than best correlating splice: n= ' + str(model_better_than_corr) + ' of: ' + str(n_model_better_than_corr) + ' p = ' +str(p_model_better_than_corr ) )
        f.write('\nsplice combination with time better than best correlating splice: n= ' + str(model_time_better_than_corr) + ' of: ' + str(n_model_better_than_corr) + ' p = ' +str(p_model_time_better_than_corr) )
        f.write('\nsplice combination with time better than splice combination: n= ' + str(model_time_better_than_model) + ' of: ' + str(n_time_better_than_model) + ' p = ' +str(p_model_time_better_than_model) )


    f,ax = plt.subplots(figsize=(2,7))
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.title('Th1 data', fontsize=18)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    sns.boxplot(data=(pd.DataFrame([GP_df.loc[ref_seq].dropna().values.flatten(),corr_df.loc[ref_seq].dropna().values.flatten(),pred_no_delay.values.flatten(), pred.flatten() ]).transpose()), ax=ax,)
    ax.set_ylabel('square residuals', fontsize=15)
    ax.set_xticklabels(['mRNA','best corr','model prediction', 'model and time'], rotation=-90)
    plt.tight_layout()
    ax.set_ylim([0, 3.5])
    plt.savefig('cv_pred_w_time_over_row_from_lasso.svg')
    plt.savefig('cv_pred_w_time_over_row_from_lasso.png')
    plt.savefig('cv_pred_w_time_over_row_from_lasso.eps')


    f,ax = plt.subplots(figsize=(2,7))
    data_tmp = pd.DataFrame([GP_df.loc[ref_seq].dropna().values.flatten(),corr_df.loc[ref_seq].dropna().values.flatten(),pred_no_delay.values.flatten(), pred.flatten() ]).transpose()
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.title('Th1 datadata', fontsize=18)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    sns.barplot(data=data_tmp, color='#850000ff', estimator=np.median)
    #ax.bar([0,1,2,3], np.median(data_tmp,0), yerr=np.std(data_tmp,0))
    #sns.boxenplot(data=(, ax=ax, color='#850000ff')
    #sns.violinplot(data=(pd.DataFrame([GP_df.loc[ref_seq].dropna().values.flatten(),corr_df.loc[ref_seq].dropna().values.flatten(),pred_no_delay.values.flatten(), pred_median.flatten() ]).transpose()), ax=ax, color='#850000ff')
    ax.set_ylabel('square residuals', fontsize=15)
    #ax.set_yscale('log')
    ax.set_xticklabels(['mRNA','best corr','model prediction', 'model and time'], rotation=-90)
    plt.tight_layout()
    ax.set_ylim([0, 1])
    plt.savefig('cv_pred_w_time_over_row_from_lasso_v3.svg')
    plt.savefig('cv_pred_w_time_over_row_from_lasso_v3.png')
    plt.savefig('cv_pred_w_time_over_row_from_lasso_v3.eps')

# =============================================================================
# if __name__ == '__main__':
#     main()
# =============================================================================
