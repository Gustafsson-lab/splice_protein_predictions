#!/usr/bin/env python3
# -*- coding: utf-8 -*-



import sklearn.linear_model as lm
import matplotlib.pyplot as plt
import scipy.stats as sts
import seaborn as sns
import pickle as pkl
import pandas as pd
import numpy as np
import scipy

import statsmodels.api as sm

import warnings
warnings.filterwarnings("ignore")


def load_data():
    prot, trans, tp_prot, tp_trans = pkl.load( open('data.p', 'rb'))

    mRNA = pd.read_csv('mRNA_expression.csv').set_index('Entrez')
    key = pd.read_csv('key_entrez_uniprot.csv').iloc[:,1:]
    mRNA = pd.merge(key, mRNA, left_on='ENTREZID',right_index=True).drop('ENTREZID', axis=1)

    prot_interp = prot.copy()
    interp = lambda x: scipy.interp(tp_trans, tp_prot, x)
    prot_interp = prot_interp.apply(interp, axis=1)
    prot_interp.columns = [str(x) + 'h' for x in tp_trans]
    Z = lambda X: (X - np.mean(X))/np.std(X)
    prot_interp = prot_interp.apply(Z, axis=1)
    mRNA.iloc[:,1:] = mRNA.iloc[:,1:].apply(Z, axis=1)

    __, i = np.unique(mRNA.UNIPROT,return_index=True)
    mRNA = mRNA.iloc[i,:]
    __, i = np.unique(prot_interp.index,return_index=True)
    prot_interp = prot_interp.iloc[i,:]


    prot_interp = prot_interp[(prot_interp.index.isin(mRNA.UNIPROT)) & (prot_interp.index.isin(trans.ACCNUM))]
    mRNA = mRNA[(mRNA.UNIPROT.isin(prot_interp.index)) & (mRNA.UNIPROT.isin(trans.ACCNUM))]
    trans = trans[(trans.ACCNUM.isin(prot_interp.index)) & (trans.ACCNUM.isin(mRNA.UNIPROT))]

    with open('data_inhouse.p', 'wb') as f:
        pkl.dump((prot_interp, mRNA, trans), f)
    return prot_interp, trans, mRNA


def train_model(trans, prot, mRNA, pred_col):
    last_point = []
    Z = lambda X: (X - np.mean(X))/np.std(X)
    used_trans = {}

    for i in range(prot.shape[0]):
        X = trans[trans.ACCNUM == prot.index[i]].iloc[:,:-1].values

        if len(X.shape) != 2:
            last_point.append(np.NaN)
            continue

        g = mRNA.iloc[i,1:].values.reshape(1,6)
        X = np.concatenate((X, g), axis=0)

        Y = prot.values[i,:]

        mod = lm.LassoCV()
        mod.fit(X[:,np.arange(6)!=pred_col].T, Y[np.arange(6)!=pred_col].T)

        if np.all(mod.coef_ == 0):
            last_point.append(np.NaN)
            continue

        used = mod.coef_ != 0

        predtmp = sm.OLS(Y[np.arange(6)!=pred_col], X[used,:][:,np.arange(6)!=pred_col].T.astype(float)).fit()
        last_point.append(Z(np.dot(X[used,:].T,predtmp.params))[pred_col])

        tnames = trans[trans.ACCNUM == prot.index[i]].index.values
        names = np.concatenate((tnames, [prot.index[i]]))
        used_trans.update({prot.index[i]:names[used]})
    return last_point, used_trans


def across_time(prot_set, mRNA_set, pred_set):
    corr_pred = []
    corr_mRNA = []
    nNaN = []

    pred_set = np.array(pred_set).T
    for i in range(len(pred_set[:,0])):
        pred_tmp = pred_set[i,:]
        mRNA_tmp = mRNA_set.values[i,:]
        prot_tmp = prot_set.values[i,:]

        isnan_tmp = np.isnan(pred_tmp)
        nNaN.append(np.sum(isnan_tmp))

        if np.any(isnan_tmp):
            pred_tmp[isnan_tmp] = mRNA_tmp[isnan_tmp]

        ctmp, ptmp = sts.spearmanr(pred_tmp, prot_tmp)
        corr_pred.append(ctmp)

        ctmp, ptmp = sts.spearmanr(mRNA_tmp, prot_tmp)
        corr_mRNA.append(ctmp)

    return corr_pred, corr_mRNA


def main():
    prot, trans, mRNA = load_data()

    preds = []
    null_resid = []
    pred_resid = []
    mRNA_resid = []
    mRNA_corr = []
    pred_corr = []
    pred_p = []
    mRNA_p = []
    used_trans = {}

    for i in range(6):
        print(i)
        p_tmp, used_trans_tmp = train_model(trans, prot, mRNA, i)
        p_tmp = np.array(p_tmp)
        preds.append(p_tmp)
        used_trans.update({prot.columns[i]:used_trans_tmp})

        mRNA_tmp = mRNA.iloc[:,i+1][~np.isnan(p_tmp)]
        prot_tmp = prot.iloc[:,i][~np.isnan(p_tmp)]


        c_tmp, prob_tmp = sts.spearmanr(p_tmp[~np.isnan(p_tmp)], prot_tmp)
        pred_corr.append(c_tmp)
        pred_p.append(prob_tmp)

        c_tmp, prob_tmp = sts.spearmanr(mRNA_tmp, prot_tmp)
        mRNA_corr.append(c_tmp)
        mRNA_p.append(prob_tmp)

        mRNA_resid.append(np.nanmedian(np.abs(prot_tmp.values - mRNA_tmp.values)))
        pred_resid.append(np.nanmedian(np.abs(prot_tmp.values - p_tmp[~np.isnan(p_tmp)])))
        null_resid.append(np.nanmedian(np.abs(prot_tmp.values)))



    with open('results.p', 'wb') as f:
        pkl.dump((preds, null_resid, pred_resid, mRNA_resid, mRNA_corr, pred_corr, pred_p, mRNA_p, used_trans), f)
    preds, null_resid, pred_resid, mRNA_resid, mRNA_corr, pred_corr, pred_p, mRNA_p, used_trans = pkl.load(open('results.p', 'rb'))

    corr_pred, corr_mRNA = across_time(prot, mRNA.iloc[:,1:], preds)

    with open('double_cv_res.txt', 'a') as f:
        f.write('pred_corr_no_l1,' + str(pred_corr)[1:-1] + '\n')

    color_pallet = "muted"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    context="notebook"
    width = 0.35
    sns.set_context(context, font_scale=1.5)

    f, ax = plt.subplots(1,2,sharey=True, figsize=(10,5))
    ax[0].hist(corr_pred)
    ax[0].set_ylabel('Freq.', fontsize=15)
    ax[0].set_xlabel('Spearman Rho', fontsize=15)
    ax[0].set_title('Predicted protein', fontsize=15)
    ax[1].set_title('mRNA', fontsize=15)
    ax[1].hist(corr_mRNA)
    f.suptitle('Predicted protein vs mRNA')
    plt.savefig('corr_over_time_pred_prot_vs_mRNA_unseen.png')

    plt.figure()
    df = pd.DataFrame(pd.Series(corr_mRNA))
    df.columns = ['Correlation mRNA']
    df['Correlation prediction'] = corr_pred
    sns.jointplot(x="Correlation mRNA", y="Correlation prediction", data=df, kind="kde")
    plt.savefig('corr_over_time_pred_prot_unseen_vs_mRNA_dependence.png')

    plt.figure()
    plt.bar(np.arange(6)-(width/2),mRNA_corr, width, label='Gene expression')
    plt.bar(np.arange(6)+(width/2),pred_corr, width, label='Predicted protein',color='g')
    plt.plot([-width,5+width],[0,0])
    plt.title('Correlation across genes', fontsize=16)
    plt.xticks(np.arange(6), ('0h', '30m', '1h', '2h', '6h', '24h'))
    plt.xlabel('Time point left out', fontsize=15)
    plt.ylabel('Spearman rho', fontsize=15)
    plt.legend()
    plt.savefig('correlation_of_predicted_genes_no_operator.svg')

    f, ax = plt.subplots(1,1,figsize=(8,6))
    plt.bar(np.arange(6)-(width/2),mRNA_corr, width, label='Gene expression')
    plt.bar(np.arange(6)+(width/2),pred_corr, width, label='Predicted protein',color='g')
    plt.plot([-width,5+width],[0,0])
    plt.title('Correlation across genes', fontsize=16)
    plt.xticks(np.arange(6), ('0h', '30m', '1h', '2h', '6h', '24h'))
    plt.yticks([0,0.2,0.4,0.6])
    ax.set_xlabel('Time point left out', fontsize=18)
    ax.set_ylabel('Spearman rho', fontsize=18)
    ax.set_ylim([-0.05,0.6])
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    plt.legend()
    plt.savefig('correlation_of_predicted_genes_no_operator_v2.svg')

    plt.figure()
    plt.bar(np.arange(6)+(width/2),-np.log10(mRNA_p), width, label='Gene expression')
    plt.bar(np.arange(6)-(width/2),-np.log10(pred_p), width, label='Predicted protein')
    plt.plot([0,5],[-np.log10(0.05),-np.log10(0.05)])
    plt.xticks(np.arange(6), ('0h', '30m', '1h', '2h', '6h', '24h'))
    plt.xlabel('Time point left out', fontsize=15)
    plt.ylabel('Spearman rho', fontsize=15)
    plt.title('Correlation across genes', fontsize=16)
    plt.legend()
    plt.savefig('pvals_of_correlation_of_predicted_genes_no_operator.png')

    _golden = (1 + (5**(.5)))/2
    f, ax = plt.subplots(1,1,figsize=(_golden*4, 4))
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_linewidth(2)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_position(('outward', 20))
    ax.tick_params(axis='both',
                   which='major',
                   length=5,
                   width=2,
                   labelsize=15,
                   )
    ax.plot([-(width*1.5), 5+(width*1.5)], [0,0], linewidth=2, color='k')
    ax.bar(np.arange(6)-(width/2),mRNA_corr, width, label='Gene expression', color='#bbbbbbff')
    ax.bar(np.arange(6)+(width/2),pred_corr, width, label='Predicted protein',color='#afe9ddff')
    ax.set_xticklabels(['', '0h', '30m', '1h', '2h', '6h', '24h', ])
    ax.set_ylabel('Spearman correlation', fontsize=18)
    ax.legend(frameon=False)
    f.savefig('correlation_of_predicted_genes_no_operator_v3.svg')
