#!/usr/in/env python3
# -*- coding: utf-8 -*-
"""


import statsmodels.sandbox.stats.multicomp as sssm
import matplotlib.pyplot as plt
import scipy.stats as sts
import seaborn as sns
import pickle as pkl
import pandas as pd
import numpy as np
import os

color_pallet = "muted"
plt.style.use('seaborn-ticks')
sns.set_color_codes(color_pallet)

context="notebook"
sns.set_context(context, font_scale=1.5)


def read_data(name):
    df_sick = pd.read_csv(name+ '/set1.csv')#.set_index('ENST')
    df_healthy = pd.read_csv(name + '/set2.csv')#.set_index('ENST')
    return df_sick, df_healthy


def read_model():
    #with open('full_model_prediction_treg.p', 'rb') as f:
    with open('build_models/Th1_model.p', 'rb') as f:
        used_trans, used_params = pkl.load(f)
# =============================================================================
#
#     # vals saved as
#     #rho_dict, rho_dict_1SE, names, coef_dict, coef_dict_1SE, R2_dict, time_cv_min, time_cv_1SE
#     with open('full_model_prediction_Th1.p', 'rb') as f:
#         tmp = pkl.load(f)
#         used_trans, used_params = tmp[2:4]
# =============================================================================

    # Convert to ENSG
    key = pd.read_csv('key_acc_gene.csv').iloc[:,1:].dropna().set_index('ACCNUM')
    used_trans_ENSG = {}
    used_params_ENSG = {}
    for up_name in used_trans.keys():
        if up_name not in used_trans.keys():
            continue

        nms = key.loc[up_name]
        if nms.shape[0] != 1:
            for i in range(nms.shape[0]):
                used_trans_ENSG.update({nms.values[i][0]:used_trans[up_name]})
                used_params_ENSG.update({nms.values[i][0]:used_params[up_name]})
        else:
            used_trans_ENSG.update({nms[0]:used_trans[up_name]})
            used_params_ENSG.update({nms[0]:used_params[up_name]})
    return used_trans_ENSG, used_params_ENSG

def make_prediction(df, used_trans, used_params):
    preds = {}
    nObs = df.shape[1] - 2

    for GNAME in df.ENSG.unique():
        df_tmp = df[df.ENSG == GNAME]

        if df_tmp.shape[0] == 0:
            continue
        elif GNAME not in used_trans:
            continue

        output = np.zeros((1, nObs))
        model_tmp = used_trans[GNAME]
        params_tmp = used_params[GNAME]
        for i in range(model_tmp.shape[0]):
            if model_tmp[i] in df_tmp.ENST.values:
                tmp_vals = df_tmp[df_tmp.ENST == model_tmp[i]].iloc[:,2:].values
                if len(tmp_vals.shape) != 1:
                    tmp_vals = np.sum(tmp_vals, axis=0)
                output += (tmp_vals * params_tmp[i])

        if np.any(output != 0):
            preds.update({GNAME:output[0]})

    out_df = pd.DataFrame(preds).transpose()
    out_df.columns = df.columns[2:]
    return out_df


def do_tests(df1, df2):
    p = []
    for i in range(df1.shape[0]):
        if len(np.unique(np.concatenate((df1.values[i,:],df2.values[i,:])) )) == 1:
            continue
        __, ptmp = sts.kruskal(df1.values[i,:], df2.values[i,:])
        p.append(ptmp)
    return p


# =============================================================================
# def print_output(df_healthy_pred, p_pred):
#     df_out = pd.DataFrame(df_healthy_pred.index)
#     df_out.columns = ['ENSEMBL']
#     df_out['p_val'] = p_pred
#     tmp = sssm.multipletests(p_pred, method='fdr_bh') # FDR 2-stage Benjamini-Hochberg
#     df_out['qval'] = tmp[1]
#
#     key = pd.read_csv('key_ensembl_symbol.csv').iloc[:,1:]
#     df_out = pd.merge(df_out, key, how='left', on='ENSEMBL').sort('p_val')
#     df_out.to_csv('results_treg.csv')
# =============================================================================
def write_some_genes(p_data_in_model, df_inmod_index, p_pred, df_pred_index):
    df_inmod_index['p_gene'] = p_data_in_model
    model_pred = pd.DataFrame(pd.Series(dict(zip(df_pred_index, p_pred))))
    model_pred.columns = ['p_pred']
    df_inmod = df_inmod_index.set_index('ENSG')

    merged = pd.merge(model_pred, df_inmod, how='inner', left_index=True, right_index=True)
    sortarg = np.argsort(merged.p_pred)

    merged = merged.iloc[sortarg, :]
    merged = merged[merged.p_pred < 0.05]

    genes = []
    for gene in merged.index:
        if not np.any(merged.loc[gene].p_gene.values < 0.05):
            genes.append(gene)
    np.unique(genes)

    pd.read_csv('key_SYMBOL_ENSEMBL2.csv').set_index('ENSEMBL').loc[np.unique(genes)]

def main_wrapper(name,used_trans, used_params):
    df_sick, df_healthy = read_data(name)


    df_sick = df_sick[df_sick.ENSG.isin(used_trans)]
    df_healthy = df_healthy[df_healthy.ENSG.isin(used_trans)]

    df_sick_pred = make_prediction(df_sick, used_trans, used_params)
    df_healthy_pred = make_prediction(df_healthy, used_trans, used_params)

    df_sick_pred = df_sick_pred[df_sick_pred.index.isin(df_healthy_pred.index)]
    df_healthy_pred = df_healthy_pred[df_healthy_pred.index.isin(df_sick_pred.index)]
    assert np.all(df_sick_pred.index == df_healthy_pred.index)

    df_h_inmod = df_healthy.iloc[:,2:][df_healthy.ENSG.isin(df_healthy_pred.index)]
    df_s_inmod = df_sick.iloc[:,2:][df_sick.ENSG.isin(df_sick_pred.index)]
    df_inmod_index = df_healthy.iloc[:,:2][df_healthy.ENSG.isin(df_healthy_pred.index)]

    p_pred = do_tests(df_healthy_pred, df_sick_pred)
    p_data = do_tests(df_healthy.iloc[:,2:], df_sick.iloc[:,2:])
    p_data_in_model = do_tests(df_h_inmod, df_s_inmod)

    write_some_genes(p_data_in_model.copy(), df_inmod_index.copy(), p_pred.copy(), df_healthy_pred.index.copy())

    enrich = (np.sum(np.array(p_pred) < 0.05)/np.sum(np.array(p_data) < 0.05)) * (len(p_data)/len(p_pred))
    print(enrich)
    tmp_pred = sssm.multipletests(p_pred, method='fdr_bh')
    print('Number FDR corrected from model: ',np.sum(tmp_pred[1]<0.3))

    tmp_data = sssm.multipletests(p_data, method='fdr_bh')
    print('Number FDR corrected from data: ',np.sum(tmp_data[1]<0.3))

    # Write the P-values
    df_out = pd.DataFrame()
    df_out['GENE'] = df_healthy_pred.index
    df_out['p'] = p_pred
    key = pd.read_csv('key_ensembl_symbol.csv').iloc[:,1:]
    df_out = pd.merge(df_out, key, left_on='GENE', right_on='ENSEMBL')
    df_out = df_out.iloc[:, [3,1]]
    df_out = df_out.iloc[np.argsort(df_out.p), :]
    df_out.to_csv('pvals_predicted/' + name + '_pvals.csv', index=False)

# =============================================================================
#     print_output(df_healthy_pred, p_pred)
#
#     f, ax = plt.subplots(1,2, figsize=(10,5))
#     ax[0].hist(p_data)
#     ax[0].set_ylabel('Freq.', fontsize=15)
#     ax[0].set_xlabel('P value', fontsize=15)
#     ax[0].set_title('Transcripts', fontsize=15)
#     ax[1].hist(p_pred)
#     ax[1].set_ylabel('Freq.', fontsize=15)
#     ax[1].set_xlabel('P value', fontsize=15)
#     ax[1].set_title('Predicted protein', fontsize=15)
#
# =============================================================================
    plt.figure() # This should not be printed, ugly hack
    mean_im = np.max(plt.hist(p_data_in_model, bins=20)[0])
    mean_pred = np.max(plt.hist(p_pred,bins=20)[0])
    mean_all= np.max(plt.hist(p_data,bins=20)[0])

    f, ax = plt.subplots(1,1, sharey=True,)
    ax.hist(p_data_in_model, bins=20, alpha=0.8, histtype='stepfilled', color='r',label='Splice variants')
    ax.set_ylabel('Freq. splice variant in model', fontsize=15, color='r')
    ax.set_xlabel('P value', fontsize=15)
    ax.set_ylim([0,mean_im*1.1])
    ax2 = ax.twinx()
    ax2.hist(p_pred, bins=20, alpha=0.8, histtype='stepfilled', label='Predicted protein', color='b')
    ax2.set_ylabel('Freq. predicted protein', fontsize=15,color='b')
    ax2.set_title('Increace in performance from model', fontsize=15)
    ax2.set_ylim([0,mean_pred*1.1])
    plt.savefig('img/enrichment_pvals_used_splice_vs_model_th1' + name + '.png')

    f, ax = plt.subplots(1,1,)
    ax.hist(p_data, bins=20, alpha=0.8, histtype='stepfilled', color='r',label='Splice variants')
    ax.set_ylabel('Freq. splice variant', fontsize=15, color='r')
    ax.set_xlabel('P value', fontsize=15)
    ax.set_ylim([0,mean_all*2])
    ax2 = ax.twinx()
    ax2.hist(p_pred, bins=20, alpha=0.8, histtype='stepfilled', label='Predicted protein', color='b')
    ax2.set_ylabel('Freq. predicted protein', fontsize=15,color='b')
    ax2.set_title('Increace in performance from model', fontsize=15)
    ax2.set_ylim([0,mean_pred*2])
    plt.savefig('img/enrichment_pvals_all_splice_vs_model_th1_' + name + '.png')

    with open('results_v2.txt', 'a') as f:
        f.write('Enrichment of significant genes from applying Th1 model to ' +name+ ': ' + str(enrich) + '\n')

def main():
    used_trans, used_params = read_model()
    used_params, used_trans = (used_trans, used_params)
    dirnames = [dirnames for dirnames in os.listdir() if '.' not in dirnames]
    main_wrapper('MS_study', used_trans, used_params )
    for name in dirnames:
        if (name == 'GSE94149') | (name == 'GSE96563')| (name[0] != 'G'):
            continue
        main_wrapper(name, used_trans, used_params )

# =============================================================================
#
# if __name__ == '__main__':
#     main()
#
#
# =============================================================================
