#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 31 09:44:29 2018

@author: rasmus
"""


__author__ = 'Rasmus Magnusson'
__COPYRIGHT__ = 'Rasmus Magnusson, 2018, Linköping'



import pandas as pd


def read_and_make_csv():
    df = pd.read_csv('GSE75011.fpkm.csv').iloc[:,1:]
    
    # Rearrange the columns
    c = df.columns.tolist()
    c = [c[-1]] + [c[-3]] + c[:-3] 
    df = df[c]
    
    df = df.set_index('ENST')
    df = df[(df.iloc[:,2:] == 0).sum(axis=1) < (0.5*(df.shape[1]-2 ))]
    
    df.to_csv('all_data.csv')
    
    return df


def main():
    df = read_and_make_csv()
    
    healthy = ['HC' in cname for cname in df.columns]
    allergic_rhinitis = ['AR' in cname for cname in df.columns]
    asthma = ['AS' in cname for cname in df.columns]
    not_healthy = ['HC' not in cname for cname in df.columns]
    
    healthy[0] = True
    allergic_rhinitis[0] = True
    asthma[0] = True
    not_healthy[0] = True
    
    set1 = df.iloc[:, healthy]
    set2 = df.iloc[:, allergic_rhinitis]
    set3 = df.iloc[:, asthma]
    set4 = df.iloc[:, not_healthy]
    
    
    set1.to_csv('set1.csv')
    set2.to_csv('set2.csv')
    set3.to_csv('set3.csv')
    set4.to_csv('set4.csv')
    

if __name__ == '__main__':
    main()