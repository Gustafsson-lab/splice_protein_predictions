#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 31 09:44:29 2018

@author: rasmus
"""


__author__ = 'Rasmus Magnusson'
__COPYRIGHT__ = 'Rasmus Magnusson, 2018, Linköping'



import pandas as pd


def read_and_make_csv():
    df = pd.read_csv('GSE86430.fpkm.csv').iloc[:,1:]
    
    # Rearrange the columns
    c = df.columns.tolist()
    c = [c[-1]] + [c[-3]] + c[:-3] 
    df = df[c]
    
    df = df.set_index('ENST')
    df = df[(df.iloc[:,2:] == 0).sum(axis=1) < (0.5*(df.shape[1]-2 ))]
    
    df.to_csv('all_data.csv')
    
    return df


def main():
    df = read_and_make_csv()
    
    lean = ['A' in cname for cname in df.columns]
    obese = ['B' in cname for cname in df.columns]
    
    lean[0] = True
    obese[0] = True
    
    set1 = df.iloc[:, lean]
    set2 = df.iloc[:, obese]
    
    set1.to_csv('set1.csv')
    set2.to_csv('set2.csv')
    

if __name__ == '__main__':
    main()